package me.sargunvohra.mcmods.composableautomation.mixinapi;

import net.minecraft.block.entity.HopperBlockEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.world.World;

public abstract class MixedInProps {

    public static final BooleanProperty HAS_GRATE = BooleanProperty.of("has_grate");

    public static boolean isGrated(Inventory inventory) {
        if (inventory instanceof HopperBlockEntity) {
            HopperBlockEntity be = (HopperBlockEntity) inventory;
            World world = be.getWorld();
            return world != null && world.getBlockState(be.getPos()).get(HAS_GRATE);
        }
        return false;
    }

    private MixedInProps() {
    }

}

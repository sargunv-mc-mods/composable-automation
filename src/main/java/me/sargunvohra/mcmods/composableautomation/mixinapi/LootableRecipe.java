package me.sargunvohra.mcmods.composableautomation.mixinapi;

import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootManager;
import net.minecraft.loot.context.LootContext;

import java.util.List;

public interface LootableRecipe {
    List<ItemStack> craftBonus(LootManager lootManager, LootContext lootContext);
}

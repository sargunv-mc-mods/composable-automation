package me.sargunvohra.mcmods.composableautomation.content

import me.sargunvohra.mcmods.composableautomation.common.GenericContainer1x1
import me.sargunvohra.mcmods.composableautomation.common.invFromTag
import me.sargunvohra.mcmods.composableautomation.common.invToTag
import net.minecraft.block.BlockState
import net.minecraft.block.InventoryProvider
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.block.entity.HopperBlockEntity
import net.minecraft.container.BlockContext
import net.minecraft.container.Container
import net.minecraft.container.NameableContainerFactory
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.inventory.BasicInventory
import net.minecraft.inventory.Inventory
import net.minecraft.inventory.SidedInventory
import net.minecraft.nbt.CompoundTag
import net.minecraft.text.Text
import net.minecraft.text.TranslatableText
import net.minecraft.util.Tickable
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IWorld
import java.util.function.Supplier

class ChuteBlockEntity(
    private val internalInv: Inventory = BasicInventory(1)
) : BlockEntity(type), Inventory by internalInv,
    NameableContainerFactory,
    Tickable {

    var transferCooldown: Int = -1

    var customName: Text? = null

    override fun getDisplayName(): Text {
        return customName ?: TranslatableText("container.composableautomation.chute")
    }

    override fun createMenu(syncId: Int, inv: PlayerInventory, player: PlayerEntity): Container {
        return GenericContainer1x1(syncId, inv, BlockContext.create(world, pos))
    }

    private fun attemptInsert(): Boolean {
        val world = world ?: return false

        val myStack = getInvStack(0)
        if (myStack.isEmpty) return false

        if (cachedState[ChuteBlock.Props.powered] && myStack.count <= 1) return false

        val outputDir = cachedState[ChuteBlock.Props.output]
        val outputInv = HopperBlockEntity.getInventoryAt(world, pos.offset(outputDir)) ?: return false

        val myStackCopy = this.getInvStack(0).copy()
        val ret = HopperBlockEntity.transfer(this, outputInv, this.takeInvStack(0, 1), outputDir.opposite)
        if (ret.isEmpty) {
            outputInv.markDirty()
            return true
        }

        this.setInvStack(0, myStackCopy)
        return false
    }

    override fun tick() {
        val world = world
        if (world == null || world.isClient) return

        transferCooldown--

        if (transferCooldown > 0) return

        transferCooldown = 0

        if (attemptInsert()) {
            transferCooldown = 8
            markDirty()
        }
    }

    override fun toTag(tag: CompoundTag): CompoundTag {
        super.toTag(tag)
        internalInv.invToTag(tag)
        tag.putInt("TransferCooldown", transferCooldown)
        return tag
    }

    override fun fromTag(tag: CompoundTag) {
        super.fromTag(tag)
        internalInv.invFromTag(tag)
        transferCooldown = tag.getInt("TransferCooldown")
    }

    override fun markDirty() {
        super.markDirty()
        internalInv.markDirty()
    }

    companion object {
        val type = BlockEntityType.Builder.create(Supplier { ChuteBlockEntity() }, ChuteBlock).build(null)!!
    }
}

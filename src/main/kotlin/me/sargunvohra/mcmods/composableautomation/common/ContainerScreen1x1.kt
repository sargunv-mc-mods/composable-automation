package me.sargunvohra.mcmods.composableautomation.common

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen
import net.minecraft.entity.player.PlayerEntity

class ContainerScreen1x1(container: GenericContainer1x1, player: PlayerEntity)
    : CottonInventoryScreen<GenericContainer1x1>(container, player)

package me.sargunvohra.mcmods.composableautomation.common

import com.mojang.authlib.GameProfile
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.world.World

class FakePlayer(world: World, name: String) : PlayerEntity(world, GameProfile(null, name)) {
    override fun isSpectator() = false
    override fun isCreative() = false
}
